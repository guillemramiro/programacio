/**
 * <h1>CSGO</h1>
 * @author Guillem Ramiro
 */

/*El Counter Strike �s un FPS d'estrategia que consisteix en 2 equips que combaten l'un contra l'altre. Tenen tot tipus d'armament i granades per acabar amb l'altre equip.
Les granades treuen 100 de vida a 0 metres de distancia, 90 a 1 metres, 80 a 2 metres ...
La vida de l'enemic no pot ser m�s gran que 100 ni m�s petita que 1
El primer par�metre �s el nombre de casos a examinar, el segon parametre �s la vida de l'enemic i el tercer parametre �s 
la distancia en metres a la que ha caigut la granada. El programa ha de retornar una V o una M segons si l'enemic esta Viu o Mort*/

import java.util.*;
public class CSGO {
static Scanner reader = new Scanner(System.in);
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		joc();
	}
	public static boolean estatVida(int vida, int distancia) {
		int malNade=100;
		for(int i=0;i<distancia;i++) {
			malNade=malNade-10;						//Saber el mal de la granada
		}
		vida=vida-malNade;
		
		if(vida==0 || vida<0) {
			return false;							//Saber si est� viu o mort
		}else {
			return true;
		}
	}
	
	public static char viuMort(int vida, int distancia) {
		char estVida = 0;
		
			if(!estatVida(vida, distancia)){
				estVida='M';
				System.out.println(estVida);			//Lletra en funci� del seu estat de vida
			}else {
				estVida='V';
				System.out.println(estVida);
			}
		return estVida;
	}
	
	public static void joc() {
		int ncasos, vida, distancia;
		ncasos=reader.nextInt();
		
		while(ncasos>0) {
		vida=reader.nextInt();
		distancia=reader.nextInt();					//Bucle segons els casos de prova que indica
		viuMort(vida, distancia);
		ncasos--;
		}
		
	}
}

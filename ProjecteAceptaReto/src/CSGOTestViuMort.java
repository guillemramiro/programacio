import static org.junit.Assert.*;
import org.junit.Test;
import java.util.Arrays;
import java.util.Collection;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class CSGOTestViuMort {
	private int num1;
	private int num2;
	private int resul;
	
	public CSGOTestViuMort(int num1, int num2, int resul) {
		this.num1 = num1;
		this.num2 = num2;
		this.resul = resul;
	}
	
	@Parameters
	public static Collection<Object[]> numeros() {
		return Arrays.asList(new Object[][] {
			{100, 0, 'M'},
			{10, 5, 'M'},
			{40, 10, 'V'},
			{90, 2, 'V'},
			{80, 2, 'M'}
		});
		
	}
	@Test
	public void testViuMort() {
		char res = CSGO.viuMort(num1,  num2);
		assertEquals(resul,  res);
	}

}

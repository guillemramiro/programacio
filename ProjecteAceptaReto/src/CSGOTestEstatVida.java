import static org.junit.Assert.*;
import org.junit.Test;
import java.util.Arrays;
import java.util.Collection;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;


@RunWith(Parameterized.class)
public class CSGOTestEstatVida {
	private int num1;
	private int num2;
	private boolean resul;
	
	public CSGOTestEstatVida (int num1, int num2, boolean resul) {
		this.num1 = num1;
		this.num2 = num2;
		this.resul = resul;
	}
	
	@Parameters
	public static Collection<Object[]> numeros() {
		return Arrays.asList(new Object[][] {
			{100, 0, false},
			{10, 5, false},
			{40, 10, true},
			{90, 2, true},
			{80, 2, false}
		});
		
	}
	@Test
	public void testEstatVida() {
		boolean res = CSGO.estatVida(num1,  num2);
		assertEquals(resul,  res);
	}
	

}
